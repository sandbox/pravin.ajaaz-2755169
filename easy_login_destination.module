<?php

/**
 * @file
 * Easy login destination module file.
 */

/**
 * Implements hook_menu().
 */
function easy_login_destination_menu() {
  $items['admin/config/easy-login-destination'] = array(
    'title' => 'Easy Login Destination',
    'title callback' => 'easy_login_destination',
    'description' => 'login_destination',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('easy_login_destination_get_form'),
    'access arguments' => array('easy login destination administer'),
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function easy_login_destination_permission() {
  return array(
    'easy login destination administer' => array(
      'title' => t('Easy login destination'),
      'description' => t('Allows people with permission to view and change their redirection path after login and logout.'),
    ),
  );
}

/**
 * Sets up calls to drupal_get_form().
 */
function easy_login_destination_get_form($form, &$form_state) {
  $form['login_destination'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#default_value' => variable_get('easy_login_destination_login_destination', ''),
    '#description' => t('If you wish the user to go to a page of your choosing, then enter the path for it here.'),
    '#title' => 'Login Destination',
  );
  $form['logout_destination'] = array(
    '#type' => 'textfield',
    '#size' => 40,
    '#default_value' => variable_get('easy_login_destination_logout_destination', ''),
    '#description' => t('If you wish the user to go to a page of your choosing, then enter the path for it here.'),
    '#title' => 'Logout Destination',
  );
  $form['original_path'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('easy_login_destination_original_path', ''),
    '#description' => t('This overrides the login destination path.'),
    '#title' => t('Redirect to original path'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Submit handler for the Easy login destination.
 *
 * Calls all submit handlers on the save button.
 */
function easy_login_destination_get_form_submit($form, &$form_state) {
  variable_set('login_destination', $form_state['values']['easy_login_destination_login_destination']);
  variable_set('logout_destination', $form_state['values']['easy_login_destination_logout_destination']);
  variable_set('original_path', $form_state['values']['easy_login_destination_original_path']);
  drupal_set_message(t('Your configuration has been saved.'));
}

/**
 * Implements hook_user_login().
 */
function easy_login_destination_user_login(&$edit, $account) {
  easy_login_destination_perform_redirect(variable_get('easy_login_destination_login_destination'));
}

/**
 * Redirect function.
 */
function easy_login_destination_perform_redirect($destination) {
  drupal_goto($destination);
}

/**
 * Implements hook_user_logout().
 */
function easy_login_destination_user_logout($account) {
  easy_login_destination_perform_redirect1(variable_get('easy_login_destination_logout_destination'));
}

/**
 * Redirect function.
 */
function easy_login_destination_perform_redirect1($destination) {
  session_destroy();
  drupal_goto($destination);
}

/**
 * Implements hook_url_outbound_alter().
 */
function easy_login_destination_url_outbound_alter(&$path, &$options, $original_path) {
  if (variable_get('easy_login_destination_original_path')) {
    $paths = &drupal_static(__FUNCTION__);
    if (!isset($paths)) {
      $paths = array();
      if (user_is_anonymous()) {
        // These only should get destinations if the user is anonymous.
        $paths = array('user', 'user/login', 'user/register', 'user/password');
      }
      if (!empty($paths)) {
        $paths = array_flip($paths);
      }
    }
    if (!empty($paths) && isset($paths[$path]) && !isset($options['query']['destination'])) {
      // Only add destination if we already have a destination path, or the
      // current path does not match the paths we want to add this too.
      if (isset($_GET['destination']) || !isset($paths[$_GET['q']])) {
        $options['query'] += drupal_get_destination();
      }
    }
  }
}
